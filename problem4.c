/*	
**	This program is a template for SP lab 3 task 4 you are  
**	required to implement toHexadecimal functions.
**	the program uses bitwise operators to compute bit set
**	and number system conversion
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*
**	function takes one integer variable and convert it into hexadecimal
**	and print the converted number on stdout. 
*/
void toHexadecimal(unsigned int);

int main()
{
	int num;
	printf("Enter the number to convert : ");		/*	taking input to convert  */
	scanf("%d",&num);
	//Call function toHexadecimal from here
	toHexadecimal(num);

	return 0;
}

void toHexadecimal(unsigned int num)
{
	//WRITE YOUR CODE HERE
	static char mask[] = "0123456789ABCDEF";

    	char *output = malloc(sizeof(unsigned) * 2 + 3);
    	strcpy(output, "0x00000000");

    	
    	int index = 9;

    	while (num > 0 ) 
    	{
        	output[index--] = mask[(num & 0xF)];
        	num >>= 4;            
    	}
	for(int i=0;i<strlen(output);i++)
		printf("%c",output[i]);
}
