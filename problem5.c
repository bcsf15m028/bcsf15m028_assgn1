/*
**	This program is a template for SP lab 3 task 5 you are 
**	required to implement its one function.
*/


#include<stdio.h>
#include<string.h>
#include <stdlib.h>

/*	
**	This function take file pointer as paramter to read content from and 
**	char pointer as an second argument which points to string to search in file
*/
void mygrep(FILE*, char*);

/*	
**	This function take file pointer as paramter to read content from and 
**	char pointer as an second argument which points to string to search in file
** 	and char pointer as an third argument to replace the string finded with it.
*/
void myreplace(FILE *fp,char *find, char * replace);

char* filename1;
int  main(int argc,char *argv[])
{


	/*	creating variables	
*/

	int behaviour;
	FILE *fp;
	char *filename=argv[1];
	char *filename1=argv[1];
	char *find=argv[2];
	char * replace;

	/*	check if mygrep is called or myreplace	
*/
	if((strcmp(argv[0],"./mygrep"))==0)
	{
		if(argc != 3)
		{
			printf("usage\t./mygrep filename <string-to-search>\n");
			exit(1);
		}

		behaviour = 0;
	}
	else if((strcmp(argv[0],"./myreplace"))==0)
	{
		if(argc != 4)
		{
			printf("\t./myreplace filename  <string-to-search> <string-to-replace>");
			exit(1);
		}
		behaviour = 1;
		replace = argv[3];
	}
	else
	{
		behaviour = -1;
	}



	/* opening file	
*/

	fp=fopen(filename,"rt");

	if(behaviour == 0)
	{
		mygrep(fp,find);		/*	caling function	
*/
	}
	else if ( behaviour == 1 )
	{
		myreplace(fp,find,replace);		/*	calling myreplace	
*/
	}
	
	fclose(fp);		/*	closing file	
*/
	return 0;
}


void mygrep(FILE *fp,char *find)
{
	char c1[100];
	char c2[100];

	/*	Add code to get strings from file	*/ 
	
	printf("Lines that Contain Given Word: \n");
	while(fgets(c1,100,fp)!=NULL)
	{
		/*	Add your code here to search a string find on string c1 readed from file	*/
		
        	char* ptr;
        	int i=0;
		strncpy(c2,c1,100);
        	ptr = strtok(c1, " ");
        	while(ptr !=NULL)
		{
			if((strcmp(ptr,find))==0)
			{
	                	fputs(c2,stdout);
			}
	                ptr = strtok(NULL, " ");
	        }

	}
}

void myreplace(FILE *fp,char *find, char * replace)
{
	char c1[500];
	char* ptr;
	FILE *out=fopen("newfile","w"); 
	while(fgets(c1,500,fp)!=NULL)
	{
		/*	Add your code here to search a string find on string c1 readed from file	*/
		ptr = strtok(c1, " ");
		while(ptr !=NULL)
		{
			if((strcmp(ptr,find))==0)
			{
        	        	fputs(replace,out);
				fputs(" ",out);
			}
			else
			{
				fputs(ptr,out);
				fputs(" ",out);
			}
              	  	ptr = strtok(NULL, " ");
        	}
	} 
	fclose(out);
}


